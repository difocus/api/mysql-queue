create table queue
(
  `id` int(11) unsigned auto_increment primary key,
  `payload` text null,
  `progress` int(3) UNSIGNED NOT NULL DEFAULT 0,
  `type` varchar(50) not null,
  `created` timestamp default current_timestamp() not null,
  `locked` timestamp default '0000-00-00 00:00:00' not null
) charset=utf8;