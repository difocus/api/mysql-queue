create table queue_log
(
	`id` int(11) unsigned auto_increment primary key,
	`payload` text null,
	`type` varchar(50) not null,
	`created` timestamp default '0000-00-00 00:00:00' not null,
	`done` timestamp default current_timestamp() not null
) charset=utf8;