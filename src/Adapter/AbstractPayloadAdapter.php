<?php

namespace ShopExpress\Queue\Adapter;

use Exception;
use RuntimeException;

/**
 * Class AbstractPayloadAdapter
 * @package ShopExpress\Queue\Adapter
 */
abstract class AbstractPayloadAdapter
{
    /**
     * Required keys in payload.
     * @var array
     */
    protected static $required = [];

    /**
     * Cast mapping.
     * @var array
     */
    protected static $casts = [];

    /**
     * @var array
     */
    protected $payload;

    /**
     * @var array
     */
    protected $casted = [];

    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->payload;
    }

    /**
     * @return string
     */
    public static function getAlias(): string
    {
        return static::$alias;
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->getArray());
    }

    /**
     * @param $name
     *
     * @throws Exception
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->payload[$name])) {
            return $this->payload[$name];
        }

        throw new RuntimeException("$name isn't set");
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
    }

    /**
     * @param $name
     */
    public function __isset($name)
    {
    }

    /**
     * PayloadAdapter constructor.
     *
     * @param array $payload
     *
     * @throws Exception
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
        $this->validatePayload($this->castPayload());
    }

    /**
     * @return array
     */
    protected function castPayload(): array
    {
        if (empty($this->casted) && !empty(static::$casts)) {
            foreach (static::$casts as $key => $type) {
                if (!isset($this->payload[$key])) continue;
                $value = $this->payload[$key];

                if ($type === 'array') {
                    if (is_string($value) && $value !== '') {
                        $this->casted[$key] = json_decode($value, true);
                    } elseif (is_array($value)) {
                        $this->casted[$key] = $value;
                    } else {
                        $this->casted[$key] = null;
                    }
                }

            }
        }

        return $this->getCasted();
    }

    /**
     * Validates payload.
     * @param array $payload
     * @return bool
     * @throws Exception
     */
    protected function validatePayload(array $payload): bool
    {
        foreach (static::$required as $requiredKey) {
            if (!isset($payload[$requiredKey])
                || (is_string($payload[$requiredKey]) && '' === $payload[$requiredKey])
                || (is_array($payload[$requiredKey]) && empty($payload[$requiredKey]))
            ) {
                throw new RuntimeException('"' . $requiredKey . '" can\'t be empty');
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getCasted(): array
    {
        return array_merge($this->payload, $this->casted);
    }
}
