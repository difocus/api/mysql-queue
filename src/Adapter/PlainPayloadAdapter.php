<?php


namespace ShopExpress\Queue\Adapter;

/**
 * Class PlainPayloadAdapter
 * @package DiCMS\Queue\Adapter
 */
class PlainPayloadAdapter extends AbstractPayloadAdapter
{
    /**
     * @var string
     */
    protected static $alias = 'plain';

    /**
     * @var array
     */
    protected static $required = [];
}
