<?php

namespace ShopExpress\Queue\Console;

use Composer\Script\Event;

class Install
{
    /**
     * @param Event $event
     */
    public static function postInstall(Event $event)
    {
        $io = $event->getIO();
        $arg = $io->askConfirmation('Enter access for tests? (yes/no, default - no)? ', false);
        if (escapeshellcmd($arg)) {
            $config = '';
            $config .= 'DB_HOST=127.0.0.1';
            $config .= "\n";
            $config .= 'DB_PORT=3306';
            $config .= "\n";
            $config .= 'DB_DRIVER=mysql';
            $config .= "\n";

            $arg = $io->ask('Database name: ');
            $config .= 'DB_DATABASE=' . escapeshellcmd($arg);
            $config .= "\n";

            $arg = $io->ask('Database login: ');
            $config .= 'DB_USERNAME=' . escapeshellcmd($arg);
            $config .= "\n";

            $arg = $io->askAndHideAnswer('Database password: ');
            $config .= 'DB_PASSWORD=' . escapeshellcmd($arg);
            $config .= "\n";
            $config .= "\n";

            file_put_contents('.env', $config);
        }

        $io->write('Successfully installed!');
    }
}
