<?php

namespace ShopExpress\Queue;

use DateTime;
use Exception;
use ShopExpress\Queue\Adapter\AbstractPayloadAdapter;
use ShopExpress\Queue\Exception\DecodeException;

/**
 * Class Entity
 * @package ShopExpress\Queue
 */
class Entity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var int
     */
    private $progress = 0;

    /**
     * @var DateTime
     */
    private $created;

    /**
     * @var DateTime
     */
    private $locked;

    /**
     * @var PayloadAdapterFactoryInterface
     */
    private $payloadAdapterFactory;

    /**
     * Entity constructor.
     * @param string|array $payload
     * @param PayloadAdapterFactoryInterface $payloadAdapterFactory
     * @param string $type
     * @param int|null $id
     * @param string|null $created
     * @param string|null $locked
     * @throws Exception
     */
    public function __construct($payload, PayloadAdapterFactoryInterface $payloadAdapterFactory, string $type, int $id = null, string $created = null, string $locked = null)
    {
        $this->id = $id;
        if (is_array($payload)) {
            $this->payload = $payload;
        } elseif (($this->payload = json_decode($payload, true)) === null) {
            throw new DecodeException();
        }
        $this->type = $type;
        $this->created = new DateTime($created);
        $this->locked = new DateTime($locked);
        $this->payloadAdapterFactory = $payloadAdapterFactory;
    }

    /**
     * @return AbstractPayloadAdapter
     * @throws Exception
     */
    public function getPayload(): AbstractPayloadAdapter
    {
        return $this->payloadAdapterFactory->create($this->type, $this->payload);
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @return DateTime
     */
    public function getLocked(): DateTime
    {
        return $this->locked;
    }

    /**
     * @return int
     */
    public function getProgress(): int
    {
        return $this->progress;
    }

    /**
     * @param DateTime $locked
     * @return Entity
     */
    public function setLocked(DateTime $locked): self
    {
        $this->locked = $locked;
        return $this;
    }

    public function unLock(): self
    {
        $this->locked = '0000-00-00 00:00:00';
        return $this;
    }

    /**
     * @param int $progress
     */
    public function setProgress(int $progress): void
    {
        $this->progress = $progress;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'progress' => $this->getProgress(),
            'created' => $this->getCreated()->format('c'),
            'locked' => $this->getLocked()->format('c'),
            'payload' => $this->getPayload()->getCasted(),
        ];
    }
}
