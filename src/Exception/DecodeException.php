<?php


namespace ShopExpress\Queue\Exception;


use Exception;

class DecodeException extends Exception
{
    public $message = 'Not valid json data in payload';
}
