<?php

namespace ShopExpress\Queue;

use Exception;
use PDO;
use PDOException;
use PDOStatement;
use Psr\Log\LoggerInterface;
use RuntimeException;
use ShopExpress\Queue\Adapter\PlainPayloadAdapter;
use ShopExpress\Queue\Exception\DecodeException;

/**
 * Class Manager
 * @package ShopExpress\Queue
 */
final class Manager
{
    use ManagerHelper;

    /**
     * @var PDO
     */
    private $db;
    /**
     * @var string
     */
    private $tableQueue;
    /**
     * @var string
     */
    private $tableLog;
    /**
     * @var string
     */
    private $type;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var PayloadAdapterFactoryInterface
     */
    private $payloadAdapterFactory;

    /**
     * Manager constructor.
     *
     * @param PDO $db
     * @param PayloadAdapterFactoryInterface $payloadAdapterFactory
     * @param string $type
     * @param string $tableQueue
     * @param string $tableLog
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        PDO $db,
        PayloadAdapterFactoryInterface $payloadAdapterFactory,
        string $type,
        string $tableQueue = 'queue',
        string $tableLog = 'queue_log',
        LoggerInterface $logger = null
    ) {
        $this->db = $db;
        $this->payloadAdapterFactory = $payloadAdapterFactory;
        $this->tableQueue = $tableQueue;
        $this->tableLog = $tableLog;
        $this->type = $type;
        $this->logger = $logger;

        if (!$this->existsQueueTables()) {
            $this->createQueueTables();
        }
    }

    /**
     * @param string $key
     * @param string $value
     * @return array
     * @throws Exception
     */
    public function find(string $key, string $value): array
    {
        $stmt = $this->db->prepare("SELECT * FROM {$this->tableQueue} WHERE `type` = :type AND payload LIKE '%\"$key\":\"$value\"%'");
        $stmt->execute([
            'type' => $this->type
        ]);

        $answer = [];
        foreach ($stmt->fetchAll() as $item) {
            $answer[] = new Entity($item['payload'], $this->payloadAdapterFactory, $this->type, $item['id'], $item['created']);
        }

        return $answer;
    }

    /**
     * @param int $id
     * @return null|Entity
     * @throws Exception
     */
    public function findById(int $id): ?Entity
    {
        $stmt = $this->db->prepare("SELECT * FROM {$this->tableQueue} WHERE `type` = :type AND `id` = :id");
        $stmt->execute([
            'type' => $this->type,
            'id' => $id
        ]);
        $item = $stmt->fetch();

        if (!$item) {
            return null;
        }

        try {
            return new Entity(
                $item['payload'],
                $this->payloadAdapterFactory,
                $item['type'],
                $item['id'],
                $item['created']
            );
        } catch (Exception $e) {
            if ($this->logger) {
                $this->logger->error('Item with id ' . $item['id'] . ' cannot be handled');
            }
        }

        return null;
    }

    /**
     * @param int $limit
     * @return Entity[]
     * @throws Exception
     */
    public function get(int $limit): array
    {
        $stmt = $this->db->prepare("SELECT * FROM {$this->tableQueue} WHERE `type` = :type ORDER BY id LIMIT {$limit}");
        $stmt->execute([
            'type' => $this->type
        ]);
        $answer = [];

        foreach ($stmt->fetchAll() as $item) {
            try {
                $answer[] = new Entity($item['payload'], $this->payloadAdapterFactory, $item['type'], $item['id'], $item['created'], $item['locked']);
            } catch (Exception $e) {
                if ($this->logger) {
                    $this->logger->error('Item with id ' . $item['id'] . ' cannot be handled');
                }
            }
        }
        return $answer;
    }

    /**
     * @param int $limit
     * @param array $filter
     * @return array
     * @throws Exception
     */
    public function take(int $limit, array $filter = []): array
    {
        $whereSql = 'WHERE `type` = :type AND created < NOW() AND locked < NOW() - interval 1 hour';
        foreach ($filter as $key => $value) {
            $whereSql .= " AND payload LIKE '%\"$key\":\"$value\"%'";
        }
        $stmt = $this->db->prepare("SELECT * FROM {$this->tableQueue} {$whereSql}  ORDER BY id ASC LIMIT {$limit} FOR UPDATE");
        $stmt->execute([
            'type' => $this->type
        ]);
        $answer = [];
        $ids = [];

        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
            try {
                $answer[] = new Entity($item['payload'], $this->payloadAdapterFactory, $item['type'], $item['id'], $item['created']);
                $ids[] = $item['id'];
            } catch (DecodeException $e) {
                $queueEntity = new Entity([$item['payload']], $this->payloadAdapterFactory, PlainPayloadAdapter::getAlias(), $item['id'], $item['created']);
                $this->ack($queueEntity, $e->getMessage());
            } catch (Exception $e) {
                if ($this->logger) {
                    $this->logger->error('Item with id ' . $item['id'] . ' cannot be handled');
                }
                $queueEntity = new Entity($item['payload'], $this->payloadAdapterFactory, PlainPayloadAdapter::getAlias(), $item['id'], $item['created']);
                $this->defer($queueEntity);
                $this->log($queueEntity, $e->getMessage());
            }
        }

        if (!empty($ids)) {
            $this->db->exec("UPDATE {$this->tableQueue} SET locked=NOW() WHERE id IN(" . implode(", ", $ids) . ")");
        }
        return $answer;
    }

    /**
     * @param int $id
     * @return null|Entity
     * @throws Exception
     */
    public function takeById(int $id): ?Entity
    {
        $stmt = $this->db->prepare("SELECT * FROM {$this->tableQueue} WHERE `type` = :type AND `id` = :id AND created < NOW() AND locked < NOW() - interval 1 hour FOR UPDATE");
        $stmt->execute([
            'type' => $this->type,
            'id' => $id
        ]);
        $item = $stmt->fetch();
        $entity = null;

        if (!$item) {
            return null;
        }

        try {
            $entity = new Entity(
                $item['payload'],
                $this->payloadAdapterFactory,
                $item['type'],
                $item['id'],
                $item['created']
            );
            $this->db->exec("UPDATE {$this->tableQueue} SET locked=NOW() WHERE id = {$item['id']}");
        } catch (Exception $e) {
            if ($this->logger) {
                $this->logger->error('Item with id ' . $item['id'] . ' cannot be handled');
            }
        }
        return $entity;
    }

    /**
     * @param Entity $entity
     * @param string|null $message
     * @return PDOStatement|bool
     * @throws Exception
     */
    public function ack(Entity $entity, ?string $message = null)
    {
        try {
            $this->log($entity, $message);
            return $this->db->exec("DELETE FROM {$this->tableQueue} WHERE id={$entity->getId()}");
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return false;
    }

    /**
     * @param Entity $entity
     * @param string|null $message
     * @return bool
     * @throws Exception
     */
    private function log(Entity $entity, ?string $message = null): bool
    {
        $stmtLog = $this->db->prepare("REPLACE INTO {$this->tableLog} (id, payload, `type`, created) VALUES (:id, :payload, :type, :created)");
        $payload = $entity->getPayload()->getArray();
        $payload += ['message' => $message ?? null];

        return $stmtLog->execute([
            'id' => $entity->getId(),
            'payload' => json_encode($payload),
            'type' => $entity->getType(),
            'created' => $entity->getCreated()->format('Y-m-d H:i:s'),
        ]);
    }

    /**
     * @param Entity|Entity[] $entities
     *
     * @throws Exception
     * @return bool
     */
    public function put($entities): bool
    {
        if ($entities instanceof Entity) {
            $entities = [$entities];
        }

        if (empty($entities)) {
            return false;
        }

        $values = array_map(static function (Entity $entity) {
            return [$entity->getPayload()->toJson(), $entity->getType(), $entity->getCreated()->format('Y-m-d H:i:s')];
        }, $entities);

        $placeholders = implode(',', array_fill(0, count($entities), '(?, ?, ?)'));
        $values = array_merge(...$values);

        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->tableQueue} (`payload`, `type`, `created`) VALUES {$placeholders}");
            return $stmt->execute($values);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return false;
    }

    /**
     * @param Entity $entity
     * @return bool
     * @throws Exception
     */
    public function update(Entity $entity): bool
    {
        $id = $entity->getId();
        if (!empty($id)) {
            $stmt = $this->db->prepare("UPDATE {$this->tableQueue} SET payload = :payload, created = :created, progress = :progress, locked = :locked WHERE id=:id");
            return $stmt->execute([
                'id' => $id,
                'payload' => $entity->getPayload()->toJson(),
                'progress' => $entity->getProgress(),
                'locked' => $entity->getLocked()->format('Y-m-d H:i:s'),
                'created' => $entity->getCreated()->format('Y-m-d H:i:s'),
            ]);
        }

        throw new RuntimeException('Entity Id isn\'t set');
    }

    /**
     * @param Entity $entity
     * @param string $interval
     * @return bool
     * @throws Exception
     */
    public function defer(Entity $entity, string $interval = '+ 1 hour'): bool
    {
        $entity->getCreated()->modify($interval);
        return $this->update($entity);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }
}
