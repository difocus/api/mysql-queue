<?php

namespace ShopExpress\Queue;

use PDOStatement;

/**
 * Trait ManagerHelper
 * @package ShopExpress\Queue
 */
trait ManagerHelper
{
    /**
     * @var string
     */
    public $QUEUE_PATH = '';
    /**
     * @var string
     */
    public $QUEUE_LOG_PATH = '';

    /**
     * @return bool
     */
    public function existsQueueTables(): bool
    {
        /* @var PDOStatement $stmt */
        $stmt = $this->db->prepare("show tables like :queue");
        $stmt->execute(['queue'=>$this->tableQueue]);
        if ($stmt->rowCount()==0) {
            return false;
        }
        $stmt = $this->db->prepare("show tables like :log");
        $stmt->execute(['log'=>$this->tableLog]);
        if ($stmt->rowCount()==0) {
            return false;
        }
        return true;
    }

    public function createQueueTables()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS {$this->tableQueue}
            (
                id int(11) unsigned auto_increment
                    primary key,
                payload mediumtext null,
                progress int(3) UNSIGNED NOT NULL DEFAULT 0,
                `type` varchar(50) not null,
                created timestamp default current_timestamp() not null,
                locked timestamp default '0000-00-00 00:00:00' not null
            ) charset=utf8;");
        $this->db->query("CREATE TABLE IF NOT EXISTS {$this->tableLog}
            (
                id int(11) unsigned auto_increment
                    primary key,
                payload mediumtext null,
                `type` varchar(50) not null,
                created timestamp default '0000-00-00 00:00:00' not null,
                done timestamp default current_timestamp() not null
            ) charset=utf8;");
    }
}