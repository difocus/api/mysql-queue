<?php

namespace ShopExpress\Queue;

use Exception;
use InvalidArgumentException;
use ShopExpress\Queue\Adapter\AbstractPayloadAdapter;
use ShopExpress\Queue\Adapter\PlainPayloadAdapter;

/**
 * Class PayloadAdapterFactory
 * @package ShopExpress\Queue
 */
class PayloadAdapterFactory implements PayloadAdapterFactoryInterface
{
    /**
     * Specifies payload adapter classes mapping for type.
     * @var array
     */
    protected static $typeClasses;

    /**
     * PayloadAdapterFactory constructor.
     */
    public function __construct()
    {
        static::$typeClasses = [
            PlainPayloadAdapter::getAlias() => PlainPayloadAdapter::class,
        ];
    }

    /**
     * @param string $type
     * @param array $payload
     *
     * @throws Exception
     * @return AbstractPayloadAdapter
     */
    public function create(string $type, array $payload): AbstractPayloadAdapter
    {
        if (empty($type) || !isset(static::$typeClasses[$type])) {
            throw new InvalidArgumentException(sprintf('Invalid value `%s` for `type` column', $type));
        }

        $className = static::$typeClasses[$type];

        return new $className($payload);
    }
}
