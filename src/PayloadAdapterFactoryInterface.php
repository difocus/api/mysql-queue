<?php

namespace ShopExpress\Queue;

use ShopExpress\Queue\Adapter\AbstractPayloadAdapter;

/**
 * Interface PayloadAdapterFactoryInterface
 * @package ShopExpress\Queue
 */
interface PayloadAdapterFactoryInterface
{
    /**
     * @param string $type
     * @param array $payload
     *
     * @return AbstractPayloadAdapter
     */
    public function create(string $type, array $payload): AbstractPayloadAdapter;

    /**
     * PayloadAdapterFactoryInterface constructor.
     */
    public function __construct();
}