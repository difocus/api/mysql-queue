<?php

namespace ShopExpress\Queue\Tests\Adapter;

use Exception;
use ShopExpress\Queue\Adapter\AbstractPayloadAdapter;

/**
 * Class ImportPayloadAdapter
 * @package ShopExpress\Queue\Adapter
 */
class ImportPayloadAdapter extends AbstractPayloadAdapter
{
    /**
     * @var array
     */
    protected static $required = [
        'file', 'fileHash', 'objType',
    ];

    /**
     * @var string
     */
    protected static $alias = 'import';

    /**
     * ImportPayloadAdapter constructor.
     *
     * @param array $payload
     *
     * @throws Exception
     */
    public function __construct(array $payload)
    {
        parent::__construct($payload);
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->payload['file'];
    }

    /**
     * @return string
     */
    public function getFileHash()
    {
        return $this->payload['fileHash'];
    }

    /**
     * @return string
     */
    public function getObjType()
    {
        return $this->payload['objType'];
    }
}