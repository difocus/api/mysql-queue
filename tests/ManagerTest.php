<?php

namespace ShopExpress\Queue\Tests;

use Exception;
use PDO;
use PHPUnit\Framework\TestCase;
use ShopExpress\Queue\Entity;
use ShopExpress\Queue\Manager;
use ShopExpress\Queue\Tests\Adapter\ImportPayloadAdapter;

/**
 * Class ManagerTest
 * @package ShopExpress\Queue\Tests
 */
class ManagerTest extends TestCase
{
    public function tearDown(): void
    {
        $config = parse_ini_file(__DIR__ . '/../.env');
        $db = new PDO("mysql:host=127.0.0.1;port=3306;dbname={$config['DB_DATABASE']}", $config['DB_USERNAME'], $config['DB_PASSWORD'], [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );

        $db->exec("TRUNCATE TABLE queue");
        $db->exec("TRUNCATE TABLE queue_log");

        parent::tearDown();
    }

    /**
     * @return PDO
     */
    public function testConfig(): PDO
    {
        $config = parse_ini_file(__DIR__ . '/../.env');
        $db = new PDO("mysql:host=127.0.0.1;port=3306;dbname={$config['DB_DATABASE']}", $config['DB_USERNAME'], $config['DB_PASSWORD'], [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );
        self::assertIsObject($db);
        return $db;
    }

    /**
     * @depends testConfig
     * @param PDO $db
     * @return Manager
     */
    public function testInstance(PDO $db): Manager
    {
        $manager = new Manager($db, new MockPayloadAdapterFactory(), ImportPayloadAdapter::getAlias());
        self::assertIsObject($manager);
        return $manager;
    }

    /**
     * @depends testConfig
     * @param PDO $db
     */
    public function testSyncTime(PDO $db): void
    {
        $stmt = $db->query("SELECT NOW()='" . date('Y-m-d H:i:s') . "'");
        self::assertEquals(1, $stmt->fetchColumn(), 'Системное время и время в базе данных не синхронизированы');
    }

    /**
     * @depends testInstance
     * @param Manager $manager
     * @throws Exception
     */
    public function testPutAckEntity(Manager $manager): void
    {
        self::assertTrue($manager->put($this->getMockEntity()));
        $entities = $manager->take(1);
        foreach ($entities as $entity) {
            self::assertEquals("testPutAckEntity.csv", $entity->getPayload()->getFile());
            $manager->ack($entity);
        }
    }

    /**
     * @depends testInstance
     * @param Manager $manager
     *
     * @throws Exception
     */
    public function testPutMassEntity(Manager $manager): void
    {
        self::assertTrue($manager->put([$this->getMockEntity(), $this->getMockEntity()]));
    }

    /**
     * @depends testInstance
     * @param Manager $manager
     * @throws Exception
     */
    public function testPutInvalidPayloadEntity(Manager $manager): void
    {
        $config = parse_ini_file(__DIR__ . '/../.env');
        $db = new PDO("mysql:host=127.0.0.1;port=3306;dbname={$config['DB_DATABASE']}", $config['DB_USERNAME'], $config['DB_PASSWORD'], [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );
        $db->exec("SET SESSION sql_mode='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'");

        self::assertTrue($manager->put($this->getMockEntity()));
        self::assertTrue($manager->put($this->getMockEntity()));
        $db->exec("UPDATE `queue` SET `payload` = 'foo' ORDER BY `id` ASC LIMIT 1");

        sleep(1);
        $entities = $manager->take(1);

        self::assertIsArray($entities);
        self::assertCount(0, $entities);

        $stmt = $db->query("SELECT COUNT(*) FROM `queue_log` WHERE `payload` LIKE '%Not valid json data in payload%'");
        self::assertEquals(1, $stmt->fetchColumn(), 'Нет логирования "Not valid json data in payload"');

        $stmt = $db->query("SELECT COUNT(*) FROM `queue` WHERE `locked` = '0000-00-00 00:00:00'");
        self::assertEquals(1, $stmt->fetchColumn(), 'Не найден активный queueEntity');
    }

    /**
     * @depends testInstance
     *
     * @param Manager $manager
     *
     * @throws Exception
     */
    public function testFindEntity(Manager $manager): void
    {
        self::assertTrue($manager->put($this->getMockEntity()));
        $entities = $manager->find("file", "testPutAckEntity.csv");
        /** @var Entity $entity */
        foreach ($entities as $entity) {
            /** @var ImportPayloadAdapter $payload */
            $payload = $entity->getPayload();
            self::assertEquals("testPutAckEntity.csv", $payload->getFile());
            $manager->ack($entity);
        }
    }

    /**
     * @depends testInstance
     *
     * @param Manager $manager
     *
     * @throws Exception
     */
    public function testAckEntity(Manager $manager): void
    {
        $entities = $manager->take(1);
        self::assertCount(0, $entities);
    }

    /**
     * @depends testInstance
     *
     * @param Manager $manager
     *
     * @throws Exception
     */
    public function testProgressEntity(Manager $manager): void
    {
        self::assertTrue($manager->put($this->getMockEntity()));
        sleep(1);
        $entities = $manager->take(1);
        self::assertCount(1, $entities);

        foreach ($entities as $entity) {
            self::assertInstanceOf(Entity::class, $entity);
            $entity->setProgress(50);

            self::assertTrue($manager->update($entity));
            self::assertEquals(50, $entity->getProgress());

            $manager->ack($entity);
        }
    }

    /**
     * @return Entity
     * @throws Exception
     */
    private function getMockEntity(): Entity
    {
        return new Entity(
            [
                "file" => "testPutAckEntity.csv",
                "fileHash" => 123,
                "fileType" => "csv",
                "objType" => 7,
                'globalParentOid' => 0,
            ],
            new MockPayloadAdapterFactory(),
            ImportPayloadAdapter::getAlias()
        );
    }
}
