<?php


namespace ShopExpress\Queue\Tests;


use ShopExpress\Queue\PayloadAdapterFactory;
use ShopExpress\Queue\Tests\Adapter\ImportPayloadAdapter;

/**
 * Class MockPayloadAdapterFactory
 * @package ShopExpress\Queue\Tests
 */
class MockPayloadAdapterFactory extends PayloadAdapterFactory
{
    /**
     * MockPayloadAdapterFactory constructor.
     */
    public function __construct()
    {
        parent::__construct();

        static::$typeClasses = array_merge(static::$typeClasses, [
            ImportPayloadAdapter::getAlias() => ImportPayloadAdapter::class,
        ]);
    }
}