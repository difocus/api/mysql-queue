<?php

namespace ShopExpress\Queue\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use ShopExpress\Queue\PayloadAdapterFactory;
use ShopExpress\Queue\Tests\Adapter\ImportPayloadAdapter;

/**
 * Class PayloadAdapterFactoryTest
 * @package ShopExpress\Queue\Tests
 */
class PayloadAdapterFactoryTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCreatingWithInvalidType(): void
    {
        $this->expectException('InvalidArgumentException');
        $payloadAdapterFactory = new PayloadAdapterFactory();
        $payloadAdapterFactory->create('foo', []);
    }

    /**
     * @throws Exception
     */
    public function testPayloadAdaptersRestrictEmptyStrings(): void
    {
        $this->expectException('RuntimeException');
        (new MockPayloadAdapterFactory())->create(ImportPayloadAdapter::getAlias(), [
            'file' => '',
            'fileType' => 'foo',
            'objType' => 'bar',
        ]);
    }
}